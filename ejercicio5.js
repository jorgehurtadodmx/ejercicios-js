// ejercicio 5. Toma nota


// declarar una variable con algo de texto (string)


var texto = "Mucho texto, poco texto";


// recorrer la cadena de texto para imprimir por consola c-o-n- g-u-i-o-n-e-s.
// usar funciones length y charAt()
// console.log(texto.length) //23 devuelve


for (i = 0; i < texto.length; i++) {
    document.write(texto.charAt(i) + "-"); //concatenar texto con guion
}