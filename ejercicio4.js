// ejercicio 4. La piramide.

//imprimir funcion que imprima numeros del 1 al 9, que el numero se repita tantas veces como sea el numero (1 1 vez, 2 2 veces..)
// 1
// 2 2
// 3 3 3
// ....
//hint: bucles
function piramide(i,repetido=0) {
    for (var i = 1; i < 10 ; i++) { //bucle incremental
        for(repetido = 0; repetido < i; repetido++){
            document.write(i); //document write, que a diferencia de console log, el write muestra en el navegador el resultado
        }
        document.write("<br>"); // salto de linea por cada document write anterior
    }
}

piramide(10);
//Basicamente, crear un bucle incremental que imprime X cantidad de numeros (i)
// tras ello crear otra variable Y que esté limitada por la primera variable X.
//Este ultimo bucle permite imprimir el numero i tantas veces como se indique. I


