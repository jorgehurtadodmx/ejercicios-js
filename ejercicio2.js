// crear funcion que clasifique ruedas de juquete siempre y cuando se indique el diametro de la misma

// diametro como parametro de función


//ejercicio 2
// condición 1: Si diametro es inferior o igual a 10 se imprime: "es una rueda para un juguete pequeño";

// condición 2: Si diametro es superior a 10 y menor de 20, se imprime: "es una rueda para un juguete mediano";

// condición 3: Si el diámetro es superior o igual a 20, se imprime: "es una rueda para un juguete grande";
//  comprobar condiciones con diferentes entradas y resultados.


function comprobarDiametro(diametro) {
    if (diametro <= 10) {
        console.log("Es una rueda para un juguete pequeño");
    }
    else if (diametro > 10 && diametro < 20) {
        console.log("Es una rueda para un juguete mediano");
    }
    else if (20 <= diametro) {
        console.log("Es una rueda para un juguete grande");
    }
    else {
        console.log("No has introducido un diametro");
    }
}

comprobarDiametro(1); //pequeño
comprobarDiametro(10) //pequeño

comprobarDiametro(11); //mediano

comprobarDiametro(20); //grande
comprobarDiametro(21); //grande

comprobarDiametro("a"); //sin diametro